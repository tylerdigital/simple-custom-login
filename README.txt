=== Plugin Name ===
Contributors: tylerdigital, croixhaug, scampanella
Tags: custom login, login background, login logo, login, wp-login, branding
Requires at least: 3.5.1
Tested up to: 3.6.1
Stable tag: 1.0

Here is a short description of the plugin.  This should be no more than 150 characters.  No markup here.

== Description ==

This is the long description.  No limit, and you can use Markdown (as well as in the following sections).

For backwards compatibility, if this section is missing, the full length of the short description will be used, and
Markdown parsed.

A few notes about the sections above:

*   "Contributors" is a comma separated list of wp.org/wp-plugins.org usernames
*   "Tags" is a comma separated list of tags that apply to the plugin
*   "Requires at least" is the lowest version that the plugin will work on
*   "Tested up to" is the highest version that you've *successfully used to test the plugin*. Note that it might work on
higher versions... this is just the highest one you've verified.
*   Stable tag should indicate the Subversion "tag" of the latest stable version, or "trunk," if you use `/trunk/` for
stable.

    Note that the `readme.txt` of the stable tag is the one that is considered the defining one for the plugin, so
if the `/trunk/readme.txt` file says that the stable tag is `4.3`, then it is `/tags/4.3/readme.txt` that'll be used
for displaying information about the plugin.  In this situation, the only thing considered from the trunk `readme.txt`
is the stable tag pointer.  Thus, if you develop in trunk, you can update the trunk `readme.txt` to reflect changes in
your in-development version, without having that information incorrectly disclosed about the current stable version
that lacks those changes -- as long as the trunk's `readme.txt` points to the correct stable tag.

    If no stable tag is provided, it is assumed that trunk is stable, but you should specify "trunk" if that's where
you put the stable version, in order to eliminate any doubt.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= Can I upload files to my theme rather than placing them in the plugin settings? =

Yes! You can place login-logo.png and login-background.jpg (or login-background.png) into your wp-content/ directory and Simple Custom Login will find them there!

If you set an image within the plugin settings, Simple Custom Login will always prefer that one, but it will check the wp-content/ directory before defaulting to a blank image.

This means you can set a default for all your new WP installs, just place images at wp-content/login-logo.png and wp-content/login-background.jpg & activate the plugin!

= Can I customize the position of the login box? =

We want to keep this plugin really simple, we chose a default location offset from the center that works well in the majority of cases. If you want to tweak the exact position of the box, this can be done with CSS (or there are more complex plugins available that might allow this control)

= Can I customize the cropping of the image on my phone/tablet/etc? =

Unfortunately not, we're using a CSS property called backround-size: cover; that works nicely on a variety of sizes. This will not be perfect for all images or all screen dimensions. Often this can be solved by finding another image :)

== Screenshots ==

1. Really simple settings
2. Customize your login screen for your company or your client's brand
3. Upload a custom background without a logo
4. It even scales down for mobile
5. Be happy to see your login screen

== Changelog ==

= 1.0 =
Initial Release

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: http://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
